# Script to run GemV in full system mode with vulnerability analysis enabled

protection=no_protection								# Protection scheme to be used
vul_analysis=yes								# Enable/disable vulnerability analsys
cpu_type=detailed								# Should be arm_detailed
num_procs=4									# Number of processors. Should be the same as fullsys.sh
num_l2=4									# Number of L2 caches
l1d_size=32kB									# Size of L1 Data cache
l1i_size=32kB									# Size of L1 Instruction cache
l2_size=1MB									# Size of L2 cache
l1d_assoc=2									# L1 Data cache associativity
l1i_assoc=2									# L1 Instuction cache associativity
l2_assoc=4									# L2 associativity
cacheline_size=64								# Size of cache line
restore_num=1									# Checkpoint restore number
outdir=../out/fs/splash2/$1/radix/restore/					# Output directory path
checkpointdir=../checkpoints/splash2/$1/radix/			# Checkpoint directory path
gemv_exec_path=./build/ALPHA/gem5.fast		# Path to gemV executable
disk_path=$DIR_ROOT/full_system_alpha/disks/linux-latest.img		# Path to disk image
kernel_path=$DIR_ROOT/full_system_alpha/binaries/vmlinux
config_path=./configs/example/fs.py		# Path to config file
mem=256MB

export M5_PATH=$DIR_ROOT/full_system_alpha

echo `date`

#$gemv_exec_path -d $outdir  $config_path --checkpoint-dir=$checkpointdir --disk-image=$disk_path --cpu-type=$cpu_type --caches --l2cache -n $num_procs --num-l2caches=$num_l2 --l1d_size=$l1d_size --l1i_size=$l1i_size --l2_size=$l2_size --l1d_assoc=$l1d_assoc --l1i_assoc=$l1i_assoc --l2_assoc=$l2_assoc --cacheline_size=$cacheline_size --vul_analysis=$vul_analysis --cache_prot=$protection --mem-size=$mem -r $restore_num --restore-with-cpu detailed

$gemv_exec_path -d $outdir  $config_path --checkpoint-dir=$checkpointdir --disk-image=$disk_path --cpu-type=$cpu_type --caches --l2cache -n $num_procs --num-l2caches=$num_l2 --l1d_size=$l1d_size --l1i_size=$l1i_size --l2_size=$l2_size --l1d_assoc=$l1d_assoc --l1i_assoc=$l1i_assoc --l2_assoc=$l2_assoc --cacheline_size=$cacheline_size --vul_analysis=$vul_analysis --cache_prot=$protection --mem-size=$mem -r $restore_num --restore-with-cpu detailed --ruby --topology=Crossbar

#$gemv_exec_path -d $outdir  $config_path --checkpoint-dir=$checkpointdir --disk-image=$disk_path --cpu-type=$cpu_type --caches --l2cache -n $num_procs --num-l2caches=$num_l2 --l1d_size=$l1d_size --l1i_size=$l1i_size --l2_size=$l2_size --l1d_assoc=$l1d_assoc --l1i_assoc=$l1i_assoc --l2_assoc=$l2_assoc --cacheline_size=$cacheline_size --mem-size=$mem --ruby --topology=Crossbar -r $restore_num --restore-with-cpu detailed

echo `date`

#$gemv_exec_path -d $outdir  $config_path --checkpoint-dir=$checkpointdir --disk-image=$disk_path --kernel=$kernel_path --cpu-type=$cpu_type --caches --l2cache -n $num_procs --num-l2caches=$num_l2 --l1d_size=$l1d_size --l1i_size=$l1i_size --l2_size=$l2_size --l1d_assoc=$l1d_assoc --l1i_assoc=$l1i_assoc --l2_assoc=$l2_assoc --cacheline_size=$cacheline_size --vul_analysis=$vul_analysis --cache_prot=$protection --mem-size=$mem --ruby --topology=Mesh -r $restore_num --restore-with-cpu detailed
