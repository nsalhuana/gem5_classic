# Script to run GemV in full system mode with vulnerability analysis enabled

protection=no_protection							# Protection scheme to use 
cpu_type=atomic					# Should be atomic
num_procs=4								# Set number of processors
outdir=../out/fs/splash2/$1/ocean/fullsys/				# Path to output directory
checkpoint_dir=../checkpoints/splash2/$1/ocean/		# Path to checkpoint directory
gemv_exec_path=./build/ALPHA/gem5.fast 	# Path to gemV executable
config_path=./configs/example/fs.py	# Path to config file
disk_path=$DIR_ROOT/full_system_alpha/disks/linux-latest.img # Path to disk image
script_path=splash_ocean.rcS
kernel_path=$DIR_ROOT/full_system_alpha/binaries/vmlinux
mem=256MB

export M5_PATH=$DIR_ROOT/full_system_alpha

cp $script_path  $outdir

echo `date`

$gemv_exec_path -d $outdir $config_path --kernel=$kernel_path --disk-image=$disk_path --script=$script_path --checkpoint-dir=$checkpoint_dir --cpu-type=$cpu_type -n $num_procs --mem-size=$mem

echo `date`
