#!/bin/bash

#POLICY=classic_memory
#POLICY=MESI_directory
POLICY=MOESI_directory

#Creates checkpoint directories
mkdir -p ../checkpoints/splash2/$POLICY/fft
mkdir -p ../checkpoints/splash2/$POLICY/lu
mkdir -p ../checkpoints/splash2/$POLICY/ocean
mkdir -p ../checkpoints/splash2/$POLICY/raytrace
mkdir -p ../checkpoints/splash2/$POLICY/radix

./fullsys_fft.sh $POLICY &&
./restore_fft.sh $POLICY

#./fullsys_lu.sh $POLICY &&
#./restore_lu.sh $POLICY

#./fullsys_ocean.sh $POLICY &&
#./restore_ocean.sh $POLICY

#./fullsys_radix.sh $POLICY &&
#./restore_radix.sh $POLICY

#./fullsys_raytrace.sh $POLICY &&
#./restore_raytrace.sh $POLICY
